import socket
import json
import time
# import pickle


def calculate_temp(data, threshold):
    for i in data:
        if abs(int(i) - int(threshold)) > 1:
            return False
    return True


def calculate_heart_rate(heart_data, low, high):
    for i in heart_data:
        if int(i) < int(low) or int(i) > int(high):
            return False
        return True


def edge():
    host = '127.0.0.1'  # The server's hostname or IP address
    port = 65433        # The port used by the server
    print("This is Edge")
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((host, port))
        # s.sendall(b'Hello, world')
        task_counter = 0
        while True:
            # print('waiting ...')
            data = s.recv(2048)
            if not data:
                break
            test_str = data.decode('utf-8').replace("'", '"')
            # print(test_str)
            json_msg = json.loads(test_str, 'utf-8')
            received_task_id = json_msg['taskID']
            task_counter = task_counter + 1
            print(task_counter, ' -- Task ID: ', json_msg['id'], 'Type: ', received_task_id)

            task_status = ''
            start_time = time.time()
            if received_task_id == 'temp':
                threshold = json_msg['original']
                temp_data = json_msg['raw_data']
                if calculate_temp(temp_data, threshold):
                    task_status = 'stable'
                else:
                    task_status = 'action'
            elif received_task_id == 'heart_rate':
                heart_data = json_msg['raw_data']
                low = json_msg['low']
                high = json_msg['high']
                if calculate_heart_rate(heart_data, low, high):
                    task_status = 'stable'
                else:
                    task_status = 'action'

            # conn.sendall(data)
            x = {
                'id': json_msg['id'],
                'TaskStatus': task_status,
                'execution time': str(time.time() - start_time)
            }
            s.sendall(json.dumps(x).encode('utf-8'))
            time.sleep(1)
