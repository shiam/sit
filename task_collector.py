import socket
import json
import time
import threading


def new_client(connection, address):
    with connection:
        print('Connected by', address)
        while True:
            # print('waiting ...')
            data = connection.recv(1024)
            if not data:
                break
            test_str = data.decode('utf-8').replace("'", '"')
            print('JSON in Buffer: ', test_str)
            json_msg = json.loads(test_str, 'utf-8')
            print('Task ID', json_msg['id'])
            # conn.sendall(data)
            x = {
                'id': json_msg['id'],
                'TaskStatus': 'complete'
            }
            connection.sendall(json.dumps(x).encode('utf-8'))

            time.sleep(1)
            if json_msg['id'] == '9':
                print('Complete Client: ', json_msg['Client ID'])


def server():
    host = '127.0.0.1'  # Standard loop back interface address (localhost)
    port = 65432        # Port to listen on (non-privileged ports are > 1023)
    print("welcome")
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind((host, port))
        s.listen(5)
        while True:
            print("waiting for new collection")
            conn, address = s.accept()
            # creating thread
            threading.Thread(target=new_client, args=(conn, address,)).start()
            # t1.start()
            time.sleep(1)
            # t1.join()
            # print("Task assigned to thread: {}".format(t1.name))
