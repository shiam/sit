from test_storage import server
from client_demo import client
from edge import edge
import json


def main():
    with open("config.json", 'r') as json_data_file:
        get_json = json.load(fp=json_data_file)

    if get_json['app'] == 'server':
        server()
    elif get_json['app'] == 'storage':
        pass
    elif get_json['app'] == 'client':
        client()
    elif get_json['app'] == 'edge':
        edge()


if __name__ == "__main__":
    main()
