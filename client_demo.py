import socket
import json
import time
# import pickle


def client():
    host = '127.0.0.1'  # The server's hostname or IP address
    port = 65432        # The port used by the server
    print("This is client 1")
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((host, port))
        # s.sendall(b'Hello, world')
        for i in range(10):
            x = {
                "id": str(i),
                "Client ID": "1"
            }
            y = json.dumps(x)
            test_str = bytes(y, 'utf-8')
            # print(test_str)
            s.sendall(test_str)
            # s.sendall(b'Hello, world')
            data = s.recv(1024)
            # print('Received', repr(data))
            test_str = data.decode('utf-8').replace("'", '"')
            json_msg = json.loads(test_str, 'utf-8')
            print('Task ID', json_msg['id'], 'Task Status: ', json_msg['TaskStatus'])
            time.sleep(1)
