import socket
import json
import time
import threading
import multiprocessing
import random

main_q = multiprocessing.Queue(1000)
IoT1_q = multiprocessing.Queue(1000)
IoT2_q = multiprocessing.Queue(1000)
complete_task_history = dict()
lock = multiprocessing.Lock()


def new_client(device_name, connection, address):
    print("IotT handler ready for ", device_name)
    with connection:
        print('Connected by', address)
        global complete_task_history
        while True:
            # print('waiting ...')
            if device_name == 'IoT1':
                if not IoT1_q.empty():
                    lock.acquire()
                    x = IoT1_q.get()
                    lock.release()
                    start_timer = time.time()
                    print('sending task to IoT one')
                    connection.sendall(json.dumps(x).encode('utf-8'))
                    data = connection.recv(1024)
                    tot_time = time.time() - start_timer
                    if not data:
                        break
                    test_str = data.decode('utf-8').replace("'", '"')
                    print('JSON in Buffer: ', test_str)
                    json_msg = json.loads(test_str, 'utf-8')
                    print('Task ID', json_msg['id'])
                    # exe_time = json_msg['execution time']
                    # latency_time = tot_time - exe_time
                    complete_task_history[(device_name, x['taskID'], len(x['raw_data']))] = (tot_time, json_msg['id'])
                    # conn.sendall(data)
                    # x = {
                    #     'id': json_msg['id'],
                    #     'TaskStatus': 'complete'
                    # }
                    if json_msg['id'] == '99':
                        print('Complete Client: ', json_msg['Client ID'])
            if device_name == 'IoT2':
                if not IoT2_q.empty():
                    lock.acquire()
                    x = IoT2_q.get()
                    lock.release()
                    start_timer = time.time()
                    connection.sendall(json.dumps(x).encode('utf-8'))
                    data = connection.recv(1024)
                    tot_time = time.time() - start_timer
                    if not data:
                        break
                    test_str = data.decode('utf-8').replace("'", '"')
                    print('JSON in Buffer: ', test_str)
                    json_msg = json.loads(test_str, 'utf-8')
                    print('Task ID', json_msg['id'])
                    # exe_time = json_msg['execution time']
                    # latency_time = tot_time - exe_time
                    complete_task_history[(device_name, x['taskID'], len(x['raw_data']))] = (tot_time, json_msg['id'])
                    # conn.sendall(data)
                    # x = {
                    #     'id': json_msg['id'],
                    #     'TaskStatus': 'complete'
                    # }
                    if json_msg['id'] == '99':
                        print('Complete Client: ', json_msg['Client ID'])
        time.sleep(1)


def generate_heart_bit_task():
    # global main_q
    print("Heart Bit")
    temp_shuffle_list = []
    for i in range(50):
        x = {
            "taskID": "heart_rate",
            "id": str(i),
            # "Client ID": "1",
            "high": "78",
            "low": "65"
        }
        heart_rate_list = []
        rand_range = random.randint(20, 40)
        for ii in range(rand_range):
            heart_rate_list.append(random.randint(60, 81))
        x['raw_data'] = heart_rate_list
        temp_shuffle_list.append(x)
    random.shuffle(temp_shuffle_list)

    for i in temp_shuffle_list:
        main_q.put(i)


def generate_temperature_task():
    # global main_q
    temp_shuffle_list = []
    for i in range(50):
        x = {
            "taskID": "temp",
            "id": str(i),
            # "Client ID": "1",
            "original": "75",
        }
        heart_rate_list = []
        rand_range = random.randint(20, 40)
        for ii in range(rand_range):
            heart_rate_list.append(random.randint(72, 78))
        x['raw_data'] = heart_rate_list
        temp_shuffle_list.append(x)

    random.shuffle(temp_shuffle_list)

    for i in temp_shuffle_list:
        main_q.put(i)


def task_unload():
    # global main_q
    print('Q size: ', main_q.qsize())
    global complete_task_history
    global lock
    while not main_q.empty():
        x = main_q.get()
        if complete_task_history.get(('IoT1', x['taskID'], len(x['raw_data'])), -1) == -1:
            lock.acquire()
            IoT1_q.put(x)
            lock.release()
        elif complete_task_history.get(('IoT2', x['taskID'], len(x['raw_data'])), -1) == -1:
            lock.acquire()
            IoT2_q.put(x)
            lock.release()
        else:
            exe_time1 = complete_task_history.get(('IoT1', x['taskID'], len(x['raw_data'])), -1)[0]
            exe_time2 = complete_task_history.get(('IoT2', x['taskID'], len(x['raw_data'])), -1)[0]
            if exe_time1 > exe_time2:
                lock.acquire()
                IoT2_q.put(x)
                lock.release()
            else:
                lock.acquire()
                IoT1_q.put(x)
                lock.release()
        time.sleep(1)
    print('performance')
    while True:
        if IoT1_q.empty() and IoT2_q.empty():
            break
        time.sleep(1)
    time.sleep(3)
    for key in complete_task_history.keys():
        value = complete_task_history.get(key, -1)
        print(key[0], ",", key[1], ",", key[2], ",", value[0], ",", value[1])
        # print(key, ": ", complete_task_history.get(key, -1))


def server():
    host = '127.0.0.1'  # Standard loop back interface address (localhost)
    port = 65433        # Port to listen on (non-privileged ports are > 1023)
    dev_counter = 0
    # global main_q

    generate_heart_bit_task()
    generate_temperature_task()
    print("welcome")
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind((host, port))
        s.listen(5)
        while True:
            device_name = 'default'
            if dev_counter == 0:
                device_name = 'IoT1'
                dev_counter = dev_counter + 1
            elif dev_counter == 1:
                device_name = 'IoT2'
                dev_counter = dev_counter + 1
            print("waiting for new collection")
            conn, address = s.accept()

            # creating thread
            threading.Thread(target=new_client, args=(device_name, conn, address)).start()
            # t1.start()
            print("Dev no: ", dev_counter)
            if dev_counter == 2:
                print("starting task un-loader")
                t2 = threading.Thread(target=task_unload, args=())
                t2.start()
                dev_counter = dev_counter + 1

            # t1.start()
            time.sleep(1)
            # t1.join()
            # print("Task assigned to thread: {}".format(t1.name))
